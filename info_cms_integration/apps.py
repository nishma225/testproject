from django.apps import AppConfig


class InfoCmsIntegrationConfig(AppConfig):
    name = 'info_cms_integration'
