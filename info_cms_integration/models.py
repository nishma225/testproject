from django.db import models
from cms.models import CMSPlugin
from info.models import Category, Apprenticeship


class CategoryPluginModel(CMSPlugin):
	category = models.ForeignKey(Category, on_delete=models.CASCADE)


class ApprenticeshipPluginModel(CMSPlugin):
	category = models.ForeignKey(Apprenticeship, on_delete=models.CASCADE)