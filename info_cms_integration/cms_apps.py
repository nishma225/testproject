from cms.app_base import CMSApp
from cms.apphook_pool import apphook_pool

@apphook_pool.register
class InfoApphook(CMSApp):
    app_name = "info"
    name = "InfoDev"

    def get_urls(self, page=None, language=None, **kwargs):
        return ["info.urls"]
