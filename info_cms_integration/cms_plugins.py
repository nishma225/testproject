from django.db.models import Q, Sum
from cms.plugin_base import CMSPluginBase
from cms.plugin_pool import plugin_pool

from .models import CategoryPluginModel, ApprenticeshipPluginModel
from django.utils.translation import ugettext as _
from info.models import Category, Job, JobCandidate, Apprenticeship, ApprenticeshipCandidate


@plugin_pool.register_plugin  # register the plugin
class CategoryPluginPublisher(CMSPluginBase):
    model = CategoryPluginModel  # model where plugin data are saved
    module = _("Category")
    name = _("Category Plugin")  # name of the plugin in the interface
    render_template = "info_cms_integration/category_plugin.html"

    # def render(self, context, instance, placeholder):
    #     context = super(CategoryPluginPublisher, self).render(context, instance, placeholder)
    #     return context

    def render(self, context, instance, placeholder):
        categories = Category.objects.annotate(
            sum_jobs=Sum(
                'jobs__no_of_vacancy',
                filter=Q(jobs__is_active=True),
            )
        )
        categories = categories.filter(is_active=True)
        # categories = Category.objects.annotate(sum_jobs=Sum('jobs__no_of_vacancy'))
        context.update({'instance': instance, 'categories': categories})
        return context


@plugin_pool.register_plugin  # register the plugin
class CategoryPluginPublisherDropDown(CMSPluginBase):
    model = CategoryPluginModel  # model where plugin data are saved
    module = _("Category")
    name = _("Category Dropdown Plugin")  # name of the plugin in the interface
    render_template = "info_cms_integration/category_plugin_dropdown.html"

    def render(self, context, instance, placeholder):
        categories = Category.objects.filter(is_active=True)
        context.update({'instance': instance, 'categories': categories})
        return context


@plugin_pool.register_plugin  # register the plugin
class ApprenticeshipPluginPublisher(CMSPluginBase):
    model = ApprenticeshipPluginModel  # model where plugin data are saved
    module = _("Apprenticeship")
    name = _("Apprenticeship Plugin")  # name of the plugin in the interface
    render_template = "info_cms_integration/apprenticeship_plugin.html"

    def render(self, context, instance, placeholder):
        apprenticeships = Apprenticeship.objects.filter(is_active=True)[:3]
        context.update({'instance': instance, 'apprenticeships': apprenticeships})
        return context
