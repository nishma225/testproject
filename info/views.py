import mimetypes

from django.conf import settings
from django.core.mail import EmailMessage
from django.db.models import Case, Count, F, IntegerField, Q, Sum, Value, When
from django.http import HttpResponseRedirect, JsonResponse
from django.shortcuts import redirect, render
from django.template.loader import render_to_string
from django.urls import reverse, reverse_lazy
from django.views.generic import (CreateView, DetailView, FormView,
                                  RedirectView, TemplateView, View)
from rest_framework import generics, status, viewsets
from rest_framework.response import Response

from .forms import ApprenticeshipCandidateForm, JobCandidateForm, SolutionForm
from .models import Apprenticeship, Category, Job, JobCandidate
# from .serializers import JobCandidateListSerializer, JobCandidateSerializer
from .tasks import email_application


# def submit_application(subject, job_name, message, form, full_name, applicant):
def submit_application(request, form):
    if form.is_valid():
        form_data = form.save(commit=False)
        form_data.save()

        job_name = Job.objects.get(id=request.POST['job']).title
        job_category_name = Category.objects.get(id=request.POST['job'])

        # Sending mail to HR email
        # attachments to HR
        files_grabbed = [settings.MEDIA_URL[1:] + str(form_data.cv),
                         settings.MEDIA_URL[1:] + str(form_data.cover_letter)]
        subject = 'Job Application Received'
        template_message = render_to_string('hr_email_template.html', {
            'full_name': request.POST['full_name'],
            'email_address': request.POST['email_address'],
            'contact_number': request.POST['contact_number'],
            'job_name': job_name,
            'job_category_name': job_category_name,
        })
        # message = 'Candidate Name: {} \nEmail : {} \nContact Number : {} \nPosition: {}'.format(
        #     request.POST['full_name'],
        #     request.POST['email_address'],
        #     request.POST['contact_number'],
        #     job_name
        # )
        email_application.delay(
            subject=subject, message=template_message, files_grabbed=files_grabbed)

        # Send email to Applicant
        subject = 'Job Application Sent'
        applicant = request.POST['email_address']
        template_message = render_to_string('candidate_email_template.html', {
            'full_name': request.POST['full_name'],
            'email_address': request.POST['email_address'],
            'contact_number': request.POST['contact_number'],
            'job_name': job_name,
            'job_category_name': job_category_name,
        })
        # message = 'Dear {},\nWe have received your application for the position of {}. You will hear from us very soon. \n\nHR Department, \nInfoDevelopers Pvt. Ltd.'.format(
        #     full_name,
        #     job_name,
        # )
        email_application.delay(
            subject=subject, message=template_message, applicant=applicant)

        return JsonResponse(
            {'message': 'Thank you for submitting. We will contact you soon', 'success': 'true'})
    else:
        print('not good')
        print(form.errors)
        return JsonResponse({'errors': form.errors, "success": 'false'})


# class SolutionFormMixin(View):
#     template_name = 'info/partials/solution_form.html'
#     form_class = SolutionForm
#
#     # def post(self, request, *args, **kwargs):
#     #     if request.is_ajax():
#     #         try:
#     #             form = self.form_class(request.POST)
#     #             form.save()
#     #             return JsonResponse({'message': 'Thank you for submitting. We will contact you soon'})
#     #         except:
#     #             return JsonResponse({'message': 'You have already submitted. We will contact you soon'})
#     #     url = reverse('home')
#     #     return HttpResponseRedirect(url)
#
#     def form_valid(self, form):
#         form.save(self.request.POST)
#         print(self.request.META['HTTP_REFERER'])
#         return super().form_valid(form)


# class SolutionFormMixin(FormView):
#     template_name = 'info/partials/solution_form.html'
#     form_class = SolutionForm
#
#     def form_valid(self, form):
#         form.save(self.request.POST)
#         return redirect(self.request.META['HTTP_REFERER'])
#         return super().form_valid(form)


# def email_application(files_grabbed, subject, message):
#     # print(files_grabbed, message[0])
#     email = EmailMessage(
#         subject=subject,
#         body=message,
#         from_email=settings.EMAIL_HOST_USER,
#         to=['ramesrest@gmail.com', ],
#         # attachments=[(cv.name, cv.read(), cv.content_type)]
#     )
#
#     email.attach_file(files_grabbed)
#     email.send()


class SolutionFormMixin(View):
    template_name = 'info/partials/solution_form.html'
    form_class = SolutionForm

    def post(self, request, *args, **kwargs):
        print(request.POST)
        email_address = request.POST['email_address']
        company_name = request.POST['company_name']
        contact_no = request.POST['contact_no']
        message = 'Email address: {} \n Company Name : {} \n Contact Number : {}'.format(email_address,
                                                                                         company_name,
                                                                                         contact_no),
        print(message)
        if request.is_ajax():
            try:
                form = self.form_class(request.POST)
                if form.is_valid():
                    form.save()
                    # send_mail(
                    #     subject="Business Enquiry Received",
                    #     message=message[0],
                    #     from_email=settings.EMAIL_HOST_USER,
                    #     recipient_list=['ramesh@yopmail.com', ],
                    #     fail_silently=False,
                    # )
                    return JsonResponse({'message': 'Thank you for submitting. We will contact you soon'})
            except:
                return JsonResponse({'error': 'Sorry, Something went wrong'})
        url = reverse('home')
        return HttpResponseRedirect(url)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['form'] = self.form_class
        return context


class HomePageView(SolutionFormMixin, TemplateView):
    template_name = 'info/home.html'


class CareersView(SolutionFormMixin, TemplateView):
    template_name = 'info/careers.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['apprenticeship_last'] = Apprenticeship.objects.last()
        return context


class ContactUsView(SolutionFormMixin, TemplateView):
    template_name = 'info/contact_us.html'


class AboutUsView(SolutionFormMixin, TemplateView):
    template_name = 'info/about_us.html'


class CareersDetailView(SolutionFormMixin, DetailView):
    model = Category
    template_name = 'info/careers_detail.html'
    context_object_name = 'category'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        # This is equivalent to below Case query
        # context['category_menu'] = Category.objects.annotate(
        #     sum_jobs=Sum(
        #         'jobs__no_of_vacancy',
        #         filter=Q(jobs__is_active=True),
        #     )
        # )
        # context['category_menu'] = Category.objects.annotate(
        #     sum_jobs=Sum(
        #         Case(
        #             When(jobs__is_active=True, then=F('jobs__no_of_vacancy')),
        #             output_field=IntegerField()
        #         )
        #     )
        # )
        category_menu = Category.objects.annotate(
            sum_jobs=Sum(
                Case(
                    When(jobs__is_active=True, then=F('jobs__no_of_vacancy')),
                    output_field=IntegerField()
                )
            )
        )
        context['category_menu'] = category_menu.filter(is_active=True)
        context['apprenticeships'] = Apprenticeship.objects.filter(
            is_active=True)
        return context


class JobDetailView(DetailView):
    model = Job
    template_name = 'info/modal_forms/job_detail_modal.html'
    context_object_name = 'job'


class JobApplyView(View):
    template_name = 'info/modal_forms/job_apply_modal.html'

    def get(self, request, *args, **kwargs):
        if request.is_ajax():
            # previous_visited_link = str(request.META['HTTP_REFERER'])
            job_id = self.kwargs['pk']
            job_name = Job.objects.values_list('title').get(pk=job_id)[0]
            form = JobCandidateForm()
            return render(request, self.template_name, {'job_id': job_id, 'form': form, 'job_name': job_name})
        url = reverse('careers')
        return HttpResponseRedirect(url)

    def post(self, request, *args, **kwargs):
        if request.is_ajax():
            print(request.POST)
            # print(request.FILES)
            try:
                form = JobCandidateForm(request.POST, request.FILES)
                return submit_application(request, form)
            except:
                print('bad')
                return JsonResponse({'errors': form.errors, "success": 'false'})
        url = reverse('careers')
        return HttpResponseRedirect(url)


class ApprenticeshipDetailView(DetailView):
    model = Apprenticeship
    template_name = 'info/modal_forms/apprenticeship_detail_modal.html'
    context_object_name = 'job'


class ApprenticeshipApplyView(View):
    template_name = 'info/modal_forms/apprenticeship_apply_modal.html'

    def get(self, request, *args, **kwargs):
        if request.is_ajax():
            # previous_visited_link = str(request.META['HTTP_REFERER'])
            job_id = self.kwargs['pk']
            job_name = Apprenticeship.objects.values_list(
                'title').get(pk=job_id)[0]
            form = ApprenticeshipCandidateForm()
            return render(request, self.template_name, {'job_id': job_id, 'form': form, 'job_name': job_name})
        url = reverse('careers')
        return HttpResponseRedirect(url)

    def post(self, request, *args, **kwargs):
        if request.is_ajax():
            subject = "Apprenticeship Application Received"
            job_name = Apprenticeship.objects.get(id=request.POST['job']).title
            message = 'Candidate Name: {} \nEmail : {} \nContact Number : {} \nPosition: {}'.format(
                request.POST['full_name'],
                request.POST['email_address'],
                request.POST['contact_number'],
                job_name
            )
            full_name = request.POST['full_name']
            applicant = request.POST['email_address']
            try:
                form = ApprenticeshipCandidateForm(request.POST, request.FILES)
                return submit_application(subject, job_name, message, form, full_name, applicant)
            except:
                return JsonResponse({'errors': form.errors, "success": 'false'})
        url = reverse('careers')
        return HttpResponseRedirect(url)


class BlogRedirectView(RedirectView):
    url = 'http://192.168.50.219'


class TermsAndConditionsView(TemplateView):
    template_name = 'info/terms_and_conditions.html'


# class JobCandidateListViewSet(generics.ListAPIView):
#     queryset = JobCandidate.objects.all()
#     serializer_class = JobCandidateListSerializer


# class JobCandidateRetriveUpdateViewSet(generics.RetrieveUpdateAPIView):
#     queryset = JobCandidate.objects.all()
#     serializer_class = JobCandidateSerializer

#     def put(self, request, *args, **kwargs):
#         pass

#     def patch(self, request, *args, **kwargs):
#         # print(request.data)
#         candidate_id = request.data.pop('id')
#         is_processed = request.data.pop('is_processed')
#         candidate = JobCandidate.objects.get(id=candidate_id)
#         candidate.is_processed = is_processed
#         candidate.save()
#         return Response(candidate)
