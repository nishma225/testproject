from django.contrib import admin
from .models import Category, Job, JobCandidate, Apprenticeship, ApprenticeshipCandidate, Solution

admin.site.register(Category)
admin.site.register(Job)
admin.site.register(JobCandidate)
admin.site.register(Apprenticeship)
admin.site.register(ApprenticeshipCandidate)
admin.site.register(Solution)
