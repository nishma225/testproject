from django import forms
from .models import JobCandidate, ApprenticeshipCandidate, Solution


class JobCandidateForm(forms.ModelForm):
    class Meta:
        model = JobCandidate
        fields = '__all__'


class ApprenticeshipCandidateForm(forms.ModelForm):
    class Meta:
        model = ApprenticeshipCandidate
        fields = '__all__'


class SolutionForm(forms.ModelForm):
    class Meta:
        model = Solution
        fields = '__all__'
