from django.db import models
from django.contrib.auth.models import User
from .validators import validate_file

JOB_LEVEL_CHOICES = (
    ('Junior', 'Junior'),
    ('Mid', 'Mid'),
    ('Senior', 'Senior')
)

EMPLOYEMENT_TYPE_CHOICES = (
    ('Full', 'Full'),
    ('Part', 'Part'),
)


class JobAbstractModel(models.Model):
    title = models.CharField(max_length=150)
    no_of_vacancy = models.PositiveIntegerField()
    offered_salary = models.CharField(max_length=200)
    deadline = models.DateTimeField()
    education_level = models.CharField(max_length=200)
    description = models.TextField(blank=True)
    description_points = models.TextField(blank=True)
    skills = models.TextField()
    other_specification = models.TextField()
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    updated_by = models.ForeignKey(User, on_delete=models.CASCADE, related_name='+')
    is_active = models.BooleanField(default=1)

    class Meta:
        abstract = True


class CandidateAbstractModel(models.Model):
    full_name = models.CharField(max_length=200, blank=False)
    email_address = models.EmailField(max_length=200, blank=False)
    contact_number = models.CharField(max_length=200, blank=False)
    applied_date = models.DateTimeField(auto_now_add=True, blank=False)
    is_processed = models.BooleanField(default=False)

    class Meta:
        abstract = True


class Category(models.Model):
    name = models.CharField(max_length=150)
    is_active = models.BooleanField(default=1)
    image = models.ImageField(upload_to='categories_images/')

    def __str__(self):
        return self.name


class Job(JobAbstractModel):
    category = models.ForeignKey(Category, on_delete=models.CASCADE, related_name='jobs')
    job_level = models.CharField(max_length=50, choices=JOB_LEVEL_CHOICES, default='Mid')
    employement_type = models.CharField(max_length=50, choices=EMPLOYEMENT_TYPE_CHOICES, default='Full')
    experienced_required = models.FloatField()
    created_by = models.ForeignKey(User, on_delete=models.CASCADE, related_name='jobs_created_by')

    def __str__(self):
        return self.title


class JobCandidate(CandidateAbstractModel):
    job = models.ForeignKey(Job, on_delete=models.SET_NULL, blank=True, null=True, related_name='job_candiates')
    cv = models.FileField(upload_to='all_cv/job_cv_files/%Y/%m/%d', blank=False, validators=[validate_file])
    cover_letter = models.FileField(upload_to='all_cover_letter/job_cover_letter_files/%Y/%m/%d', blank=False,
                                    validators=[validate_file])

    def __str__(self):
        return self.email_address

    class Meta:
        unique_together = ('email_address', 'job')


class Apprenticeship(JobAbstractModel):
    category = models.ForeignKey(Category, on_delete=models.CASCADE, related_name='apprenticeships')
    deadline = models.DateTimeField()
    created_by = models.ForeignKey(User, on_delete=models.CASCADE, related_name='apprenticeships_created_by')

    def __str__(self):
        return self.title


class ApprenticeshipCandidate(CandidateAbstractModel):
    job = models.ForeignKey(Apprenticeship, on_delete=models.SET_NULL, blank=True, null=True,
                            related_name='apprenticeship_candiates')
    cv = models.FileField(upload_to='all_cv/apprenticeship_cv_files/%Y/%m/%d', blank=False, validators=[validate_file])
    cover_letter = models.FileField(upload_to='all_cover_letter/apprenticeship_cover_letter_files/%Y/%m/%d',
                                    blank=False, validators=[validate_file])

    def __str__(self):
        return self.email_address

    class Meta:
        unique_together = ('email_address', 'job')


class Solution(models.Model):
    email_address = models.EmailField()
    company_name = models.CharField(max_length=200)
    contact_no = models.CharField(max_length=200)

    def __str__(self):
        return '{} | Company Name : {}'.format(self.email_address, self.company_name)
