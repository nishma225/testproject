from django import template
from django.utils.html import conditional_escape
from django.utils.safestring import mark_safe

register = template.Library()


@register.filter(name='jobs_filter')
def jobs_filter(jobs):
    return jobs.filter(is_active=True)


@register.filter(name='job_description_spliter')
def job_description_spliter(jobs):
    job_descriptions = jobs.splitlines()
    # print(jobs.splitlines())
    return job_descriptions
