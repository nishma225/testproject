# Generated by Django 2.1.13 on 2020-01-13 06:47

from django.db import migrations, models
import django.utils.timezone
import info.validators


class Migration(migrations.Migration):

    dependencies = [
        ('info', '0003_auto_20191216_1034'),
    ]

    operations = [
        migrations.AddField(
            model_name='apprenticeshipcandidate',
            name='cover_letter',
            field=models.FileField(default=1, upload_to='all_cover_letter/apprenticeship_cover_letter_files/%Y/%m/%d', validators=[info.validators.validate_file]),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='jobcandidate',
            name='cover_letter',
            field=models.FileField(default=django.utils.timezone.now, upload_to='all_cover_letter/job_cover_letter_files/%Y/%m/%d', validators=[info.validators.validate_file]),
            preserve_default=False,
        ),
    ]
