# Generated by Django 2.1.13 on 2019-12-13 06:26

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion
import info.validators


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Apprenticeship',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('title', models.CharField(max_length=150)),
                ('no_of_vacancy', models.PositiveIntegerField()),
                ('offered_salary', models.CharField(max_length=200)),
                ('education_level', models.CharField(max_length=200)),
                ('description_paragraph', models.TextField(blank=True)),
                ('description_points', models.TextField(blank=True)),
                ('skills', models.TextField()),
                ('other_specification', models.TextField()),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('updated_at', models.DateTimeField(auto_now=True)),
                ('is_active', models.BooleanField(default=1)),
                ('deadline', models.DateTimeField()),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='ApprenticeshipCandidate',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('full_name', models.CharField(max_length=200)),
                ('email_address', models.EmailField(max_length=200)),
                ('contact_number', models.CharField(max_length=200)),
                ('applied_date', models.DateTimeField(auto_now_add=True)),
                ('cv', models.FileField(upload_to='all_cv/apprenticeship_cv_files/%Y/%m/%d', validators=[info.validators.validate_file])),
                ('job', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='apprenticeship_candiates', to='info.Apprenticeship')),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='Category',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=150)),
                ('is_active', models.BooleanField(default=1)),
                ('image', models.ImageField(upload_to='categories_images/')),
            ],
        ),
        migrations.CreateModel(
            name='Job',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('title', models.CharField(max_length=150)),
                ('no_of_vacancy', models.PositiveIntegerField()),
                ('offered_salary', models.CharField(max_length=200)),
                ('deadline', models.DateTimeField()),
                ('education_level', models.CharField(max_length=200)),
                ('description_paragraph', models.TextField(blank=True)),
                ('description_points', models.TextField(blank=True)),
                ('skills', models.TextField()),
                ('other_specification', models.TextField()),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('updated_at', models.DateTimeField(auto_now=True)),
                ('is_active', models.BooleanField(default=1)),
                ('job_level', models.CharField(choices=[('Junior', 'Junior'), ('Mid', 'Mid'), ('Senior', 'Senior')], default='Mid', max_length=50)),
                ('employement_type', models.CharField(choices=[('Full', 'Full'), ('Part', 'Part')], default='Full', max_length=50)),
                ('experienced_required', models.FloatField()),
                ('category', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='jobs', to='info.Category')),
                ('created_by', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='jobs_created_by', to=settings.AUTH_USER_MODEL)),
                ('updated_by', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='+', to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='JobCandidate',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('full_name', models.CharField(max_length=200)),
                ('email_address', models.EmailField(max_length=200)),
                ('contact_number', models.CharField(max_length=200)),
                ('applied_date', models.DateTimeField(auto_now_add=True)),
                ('cv', models.FileField(upload_to='all_cv/job_cv_files/%Y/%m/%d', validators=[info.validators.validate_file])),
                ('job', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='job_candiates', to='info.Job')),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='Solution',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('email_address', models.EmailField(max_length=254)),
                ('company_name', models.CharField(max_length=200)),
                ('contact_no', models.CharField(max_length=200)),
            ],
        ),
        migrations.AddField(
            model_name='apprenticeship',
            name='category',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='apprenticeships', to='info.Category'),
        ),
        migrations.AddField(
            model_name='apprenticeship',
            name='created_by',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='apprenticeships_created_by', to=settings.AUTH_USER_MODEL),
        ),
        migrations.AddField(
            model_name='apprenticeship',
            name='updated_by',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='+', to=settings.AUTH_USER_MODEL),
        ),
    ]
