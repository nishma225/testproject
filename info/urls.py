from django.urls import path, include
# from django.views.generic.simple import redirect_to

from . import views

urlpatterns = [
    path('', views.HomePageView.as_view(), name='home'),
    path('careers/', views.CareersView.as_view(), name='careers'),
    path('contact-us/', views.ContactUsView.as_view(), name='contact_us'),
    path('about-us/', views.AboutUsView.as_view(), name='about_us'),
    path('careers-detail/<int:pk>/',
         views.CareersDetailView.as_view(), name='careers-detail'),
    path('job-detail/<int:pk>/', views.JobDetailView.as_view(), name='job-detail'),
    path('job-apply/<int:pk>/', views.JobApplyView.as_view(), name='job-apply'),
    path('apprenticeship-detail/<int:pk>/',
         views.ApprenticeshipDetailView.as_view(), name='apprenticeship-detail'),
    path('apprenticeship-apply/<int:pk>/',
         views.ApprenticeshipApplyView.as_view(), name='apprenticeship-apply'),
    path('solution/', views.SolutionFormMixin.as_view(), name='solution-form'),
    path('blog/', views.BlogRedirectView.as_view(), name='blog_url'),
    path('apps/terms/', views.TermsAndConditionsView.as_view(), name='terms_and_conditions'),

    # api urls
#     path('api/job-candidates/', views.JobCandidateListViewSet.as_view(),
#          name='candidate-list'),
#     path('api/job-candidate/partial-update/<int:pk>/',
#          views.JobCandidateRetriveUpdateViewSet.as_view(), name='candidate-detail'),

]
