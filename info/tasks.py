# Create your tasks here
from __future__ import absolute_import, unicode_literals
from pprint import pprint
import requests
import os
import json
from collections import defaultdict
from email.mime.image import MIMEImage

from celery import task, shared_task
from django.http import JsonResponse
from django.conf import settings
from django.core.mail import EmailMessage, EmailMultiAlternatives

from .models import JobCandidate
# from ..myproject.settings.production import EMAIL_HOST_USER


@shared_task
def email_application(subject, message, files_grabbed=None, applicant=None):
    print(files_grabbed, message[0], applicant)
    # email to HR
    if files_grabbed is not None:
        # email = EmailMessage(
        #     subject=subject,
        #     body=message,
        #     from_email=settings.EMAIL_HOST_USER,
        #     to=['hr@infodeveloper.com.np', ],
        #     # to=['ramesrest@gmail.com', ],
        #     # attachments=[(cv.name, cv.read(), cv.content_type)]
        # )
        email = EmailMultiAlternatives(
            subject=subject,
            body=message,
            from_email=settings.EMAIL_HOST_USER,
            to=['hr@infodevelopers.com.np', ],
            # to=['ramesrest@gmail.com', ],
        )
        for f in files_grabbed:
            email.attach_file(f)

    # success email to an applicant
    else:
        # email = EmailMessage(
        #     subject=subject,
        #     body=message,
        #     from_email=settings.EMAIL_HOST_USER,
        #     to=[applicant],
        # )
        email = EmailMultiAlternatives(
            subject=subject,
            body=message,
            from_email=settings.EMAIL_HOST_USER,
            to=[applicant]
        )

    # InfoDev Banner
    banner_path = os.path.join(
        settings.BASE_DIR, '../static/images/info-logo.png')
    with open(banner_path, 'rb') as banner_image:
        banner_image = MIMEImage(banner_image.read())
        banner_image.add_header('Content-ID', '<logo.png>')
        email.attach(banner_image)

    # Email type : html
    email.content_subtype = 'html'
    email.mixed_subtype = 'related'
    email.send()


@shared_task
def api_post_job_candidate():
    # job_candidates = JobCandidate.objects.select_related('job__employement_type').filter(is_processed=False).values()
    job_candidates = JobCandidate.objects.filter(is_processed=False)
    if job_candidates:
        for job_candidate in job_candidates:
            print(job_candidate)
            api_data = {}
            api_data['fullName'] = None, job_candidate.full_name
            api_data['email'] = None, job_candidate.email_address
            api_data['contactNumber'] = None, job_candidate.contact_number
            
            # cover letter
            cover_letter_base_name = os.path.basename(job_candidate.cover_letter.url) # eg. returns mero_cover_letter.pdf
            cover_letter_file_name, cover_letter_extension_name = os.path.splitext(cover_letter_base_name)
            api_data['coverLetter'] = cover_letter_file_name + cover_letter_extension_name, (job_candidate.cover_letter or 'No cover Letter')

            # cv
            cv_base_name = os.path.basename(job_candidate.cv.url) # eg. returns mero_cv.pdf
            cv_file_name, cv_extension_name = os.path.splitext(cv_base_name)
            api_data['cv'] = cv_file_name + cv_extension_name, (job_candidate.cv or 'No CV attached')
            

            api_data['jobLevel'] = None, job_candidate.job.job_level
            api_data['category'] = None, job_candidate.job.category.name
            api_data['employement_type'] = None, job_candidate.job.employement_type

            headers = {
                'content-type': "multipart/form-data; boundary=----WebKitFormBoundary7MA4YWxkTrZu0gW",
            }

            # url = "http://192.168.50.65:8081/api/career/save"
            url = "http://139.5.71.109:3199/api/career/save"

            # print(api_data)

            response = requests.post(url, files=api_data)
            print(response.text)

            r = json.loads(response.text)
            # print(r['status'])

            print(response.status_code)

            # if response.status_code and r['status'] == 201:
            if response.status_code:
                job_candidate.is_processed = True
                job_candidate.save()
                print('done...')

            else:
                print('something went wrong...')
