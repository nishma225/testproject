# InfoDev Official Website

# Table of Contents
* [Basic Information](#basic-information)
* [Installation](#installation)
* [Important Folders and their Description](#important-folders-and-their-description)
* [References](#references)
* [Contact Information](#contact-information)

## Basic Information
* URL Address: https://www.infodev.com.np/
* Technologies used
  + django-cms 3.7.0
  + PostgreSQL
  + jquery-3.1.0.js
* GitHub link for [django CMS](https://github.com/divio/django-cms)

## Installation
Run below command in django shell
```python
sudo apt-get install python3-pip
sudo pip3 install virtualenv 
virtualenv venv 
virtualenv -p /usr/bin/python3 venv
source venv/bin/activate
pip install -r requirements.txt
celery -A myproject worker -l info
python manage.py makemigrations
python manage.py migrate
python manage.py runserver 8000
```

## Important Folders and their Description
* info: Main app of this project
* info_cms_integration: Glue together both django-cms & info app
* templatetags: Contains all the template tags related to app

## References:
    * http://docs.django-cms.org/en/latest/

## Contact Information:
For any further queries regarding Django CMS and its use in Info Developers, please
contact at:  
Email: ramesrest@gmail.com  
Phone Number: +9779860298534

For query related for updating on server:  
Sagar Limbu  
Email: sagar.limbu@infodevelopers.com.np  
Phone Number: +977-9823166455

**************************
__Development __   
[Ramesh Pradhan](mailto:ramesrest@gmail.com)
