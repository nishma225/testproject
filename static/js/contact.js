/******/ (function(modules) { // webpackBootstrap
/******/ 	// install a JSONP callback for chunk loading
/******/ 	function webpackJsonpCallback(data) {
/******/ 		var chunkIds = data[0];
/******/ 		var moreModules = data[1];
/******/ 		var executeModules = data[2];
/******/
/******/ 		// add "moreModules" to the modules object,
/******/ 		// then flag all "chunkIds" as loaded and fire callback
/******/ 		var moduleId, chunkId, i = 0, resolves = [];
/******/ 		for(;i < chunkIds.length; i++) {
/******/ 			chunkId = chunkIds[i];
/******/ 			if(Object.prototype.hasOwnProperty.call(installedChunks, chunkId) && installedChunks[chunkId]) {
/******/ 				resolves.push(installedChunks[chunkId][0]);
/******/ 			}
/******/ 			installedChunks[chunkId] = 0;
/******/ 		}
/******/ 		for(moduleId in moreModules) {
/******/ 			if(Object.prototype.hasOwnProperty.call(moreModules, moduleId)) {
/******/ 				modules[moduleId] = moreModules[moduleId];
/******/ 			}
/******/ 		}
/******/ 		if(parentJsonpFunction) parentJsonpFunction(data);
/******/
/******/ 		while(resolves.length) {
/******/ 			resolves.shift()();
/******/ 		}
/******/
/******/ 		// add entry modules from loaded chunk to deferred list
/******/ 		deferredModules.push.apply(deferredModules, executeModules || []);
/******/
/******/ 		// run deferred modules when all chunks ready
/******/ 		return checkDeferredModules();
/******/ 	};
/******/ 	function checkDeferredModules() {
/******/ 		var result;
/******/ 		for(var i = 0; i < deferredModules.length; i++) {
/******/ 			var deferredModule = deferredModules[i];
/******/ 			var fulfilled = true;
/******/ 			for(var j = 1; j < deferredModule.length; j++) {
/******/ 				var depId = deferredModule[j];
/******/ 				if(installedChunks[depId] !== 0) fulfilled = false;
/******/ 			}
/******/ 			if(fulfilled) {
/******/ 				deferredModules.splice(i--, 1);
/******/ 				result = __webpack_require__(__webpack_require__.s = deferredModule[0]);
/******/ 			}
/******/ 		}
/******/
/******/ 		return result;
/******/ 	}
/******/
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// object to store loaded and loading chunks
/******/ 	// undefined = chunk not loaded, null = chunk preloaded/prefetched
/******/ 	// Promise = chunk loading, 0 = chunk loaded
/******/ 	var installedChunks = {
/******/ 		"contact": 0
/******/ 	};
/******/
/******/ 	var deferredModules = [];
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	var jsonpArray = window["webpackJsonp"] = window["webpackJsonp"] || [];
/******/ 	var oldJsonpFunction = jsonpArray.push.bind(jsonpArray);
/******/ 	jsonpArray.push = webpackJsonpCallback;
/******/ 	jsonpArray = jsonpArray.slice();
/******/ 	for(var i = 0; i < jsonpArray.length; i++) webpackJsonpCallback(jsonpArray[i]);
/******/ 	var parentJsonpFunction = oldJsonpFunction;
/******/
/******/
/******/ 	// add entry module to deferred list
/******/ 	deferredModules.push(["./src/js/contact.js","vendors"]);
/******/ 	// run deferred modules when ready
/******/ 	return checkDeferredModules();
/******/ })
/************************************************************************/
/******/ ({

/***/ "./src/js/components/Animation.js":
/*!****************************************!*\
  !*** ./src/js/components/Animation.js ***!
  \****************************************/
/*! exports provided: ClientAnimation, DialogAnimation, ProjectAnimation, VisionAnimation, ExpertAnimation, SlickAnimation, OurClientAnimation, BusinessAnimation, FooterLogoAnimation, AboutAnimation, AboutHeaderAnimation, ValuesAnimation */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"ClientAnimation\", function() { return ClientAnimation; });\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"DialogAnimation\", function() { return DialogAnimation; });\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"ProjectAnimation\", function() { return ProjectAnimation; });\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"VisionAnimation\", function() { return VisionAnimation; });\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"ExpertAnimation\", function() { return ExpertAnimation; });\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"SlickAnimation\", function() { return SlickAnimation; });\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"OurClientAnimation\", function() { return OurClientAnimation; });\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"BusinessAnimation\", function() { return BusinessAnimation; });\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"FooterLogoAnimation\", function() { return FooterLogoAnimation; });\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"AboutAnimation\", function() { return AboutAnimation; });\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"AboutHeaderAnimation\", function() { return AboutHeaderAnimation; });\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"ValuesAnimation\", function() { return ValuesAnimation; });\n/* harmony import */ var gsap__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! gsap */ \"./node_modules/gsap/index.js\");\n/* harmony import */ var scrollmagic__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! scrollmagic */ \"./node_modules/scrollmagic/scrollmagic/uncompressed/ScrollMagic.js\");\n/* harmony import */ var scrollmagic__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(scrollmagic__WEBPACK_IMPORTED_MODULE_1__);\n/* harmony import */ var imports_loader_define_false_scrollmagic_scrollmagic_uncompressed_plugins_animation_gsap__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! imports-loader?define=>false!scrollmagic/scrollmagic/uncompressed/plugins/animation.gsap */ \"./node_modules/imports-loader/index.js?define=>false!./node_modules/scrollmagic/scrollmagic/uncompressed/plugins/animation.gsap.js\");\n/* harmony import */ var imports_loader_define_false_scrollmagic_scrollmagic_uncompressed_plugins_animation_gsap__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(imports_loader_define_false_scrollmagic_scrollmagic_uncompressed_plugins_animation_gsap__WEBPACK_IMPORTED_MODULE_2__);\n/* harmony import */ var imports_loader_define_false_scrollmagic_scrollmagic_uncompressed_plugins_debug_addIndicators__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! imports-loader?define=>false!scrollmagic/scrollmagic/uncompressed/plugins/debug.addIndicators */ \"./node_modules/imports-loader/index.js?define=>false!./node_modules/scrollmagic/scrollmagic/uncompressed/plugins/debug.addIndicators.js\");\n/* harmony import */ var imports_loader_define_false_scrollmagic_scrollmagic_uncompressed_plugins_debug_addIndicators__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(imports_loader_define_false_scrollmagic_scrollmagic_uncompressed_plugins_debug_addIndicators__WEBPACK_IMPORTED_MODULE_3__);\n\n\n\n //contact section dialog animation\n\nvar DialogAnimation = function DialogAnimation() {\n  new gsap__WEBPACK_IMPORTED_MODULE_0__[\"TimelineMax\"]({\n    repeat: -1\n  }).to(\".contact--request\", 1.5, {\n    ease: Power2.easeIn,\n    opacity: 0\n  }).to(\".contact--response\", 1, {\n    ease: Power4.easeOut,\n    opacity: 1\n  });\n}; // CLientAnimation\n\n\nvar ClientAnimation = function ClientAnimation() {\n  var tl = new gsap__WEBPACK_IMPORTED_MODULE_0__[\"TimelineMax\"]();\n  tl.add(gsap__WEBPACK_IMPORTED_MODULE_0__[\"TweenMax\"].staggerFrom(\".client--img\", 1, {\n    scale: 1.02,\n    opacity: 0,\n    ease: Power4.easeOut,\n    stagger: {\n      from: \"center\"\n    }\n  }, 0.05));\n  var controller = new scrollmagic__WEBPACK_IMPORTED_MODULE_1___default.a.Controller();\n  var scene2 = new scrollmagic__WEBPACK_IMPORTED_MODULE_1___default.a.Scene({\n    triggerElement: \".section-client\",\n    triggerHook: 0.5\n  }).setTween(tl).addTo(controller);\n}; // Project Animation\n\n\nvar ProjectAnimation = function ProjectAnimation() {\n  var tween2 = new gsap__WEBPACK_IMPORTED_MODULE_0__[\"TimelineMax\"]();\n  tween2.fromTo(\"#milestone-title\", 1, {\n    y: -100,\n    opacity: 0\n  }, {\n    y: 0,\n    opacity: 1\n  }).fromTo(\"#project-completed\", 0.8, {\n    x: -100,\n    opacity: 0\n  }, {\n    x: 0,\n    opacity: 1\n  }, \"-=.8\").fromTo(\"#project-professional\", 0.8, {\n    y: 100,\n    opacity: 0\n  }, {\n    y: 0,\n    opacity: 1\n  }, \"-=.8\").fromTo(\"#project-operation\", 0.8, {\n    y: -100,\n    opacity: 0\n  }, {\n    y: 0,\n    opacity: 1\n  }, \"-=.8\").fromTo(\"#project-client\", 0.8, {\n    x: 100,\n    opacity: 0\n  }, {\n    x: 0,\n    opacity: 1\n  }, \"-=.8\");\n  var valuescontroller2 = new scrollmagic__WEBPACK_IMPORTED_MODULE_1___default.a.Controller();\n  var scene2 = new scrollmagic__WEBPACK_IMPORTED_MODULE_1___default.a.Scene({\n    triggerElement: \".section-milestone \",\n    triggerHook: 0.8\n  }).setTween(tween2).offset(50) // .addIndicators()\n  .addTo(valuescontroller2);\n}; // Vision Animation\n\n\nvar VisionAnimation = function VisionAnimation() {\n  var tween1 = new gsap__WEBPACK_IMPORTED_MODULE_0__[\"TimelineMax\"]();\n  tween1.fromTo(\"#mission\", 1, {\n    y: -100,\n    opacity: 0\n  }, {\n    y: 0,\n    opacity: 1\n  }).fromTo(\"#vision\", 0.8, {\n    x: -100,\n    opacity: 0\n  }, {\n    x: 0,\n    opacity: 1\n  }, \"-=.8\").fromTo(\"#milestone-img\", 0.8, {\n    x: -100,\n    opacity: 0\n  }, {\n    x: 0,\n    opacity: 1\n  }, \"-=.8\").fromTo(\"#laptopimage\", 0.8, {\n    y: 100,\n    opacity: 0\n  }, {\n    y: 0,\n    opacity: 1\n  }, \"-=.8\");\n  var valuescontroller1 = new scrollmagic__WEBPACK_IMPORTED_MODULE_1___default.a.Controller();\n  var scene1 = new scrollmagic__WEBPACK_IMPORTED_MODULE_1___default.a.Scene({\n    triggerElement: \".section-vision \",\n    triggerHook: 0.8\n  }).setTween(tween1).offset(50) // .addIndicators()\n  .addTo(valuescontroller1);\n}; // Expert Animation\n\n\nvar ExpertAnimation = function ExpertAnimation() {\n  var tween3 = new gsap__WEBPACK_IMPORTED_MODULE_0__[\"TimelineMax\"]();\n  tween3.fromTo(\"#expertise-enterprise\", 0.8, {\n    x: -100,\n    opacity: 0\n  }, {\n    x: 0,\n    opacity: 1\n  }, \"-=.8\").fromTo(\"#expertise-vr\", 0.8, {\n    y: -100,\n    opacity: 0\n  }, {\n    y: 0,\n    opacity: 1\n  }, \"-=.8\").fromTo(\"#expertise-datascience\", 0.8, {\n    x: 100,\n    opacity: 0\n  }, {\n    x: 0,\n    opacity: 1\n  }, \"-=.8\").fromTo(\"#expertise-ai\", 0.8, {\n    x: 100,\n    opacity: 0\n  }, {\n    x: 0,\n    opacity: 1\n  }, \"-=.8\").fromTo(\"#expertise-iot\", 0.8, {\n    y: 100,\n    opacity: 0\n  }, {\n    y: 0,\n    opacity: 1\n  }, \"-=.8\").fromTo(\"#expertise-bi\", 0.8, {\n    x: -100,\n    opacity: 0\n  }, {\n    x: 0,\n    opacity: 1\n  }, \"-=.8\");\n  var valuescontroller3 = new scrollmagic__WEBPACK_IMPORTED_MODULE_1___default.a.Controller();\n  var scene3 = new scrollmagic__WEBPACK_IMPORTED_MODULE_1___default.a.Scene({\n    triggerElement: \".section-expertise \",\n    triggerHook: 0.6\n  }).setTween(tween3).offset(100) // .addIndicators()\n  .addTo(valuescontroller3);\n}; // Slick Animation\n\n\nvar SlickAnimation = function SlickAnimation() {\n  var slicktween = new gsap__WEBPACK_IMPORTED_MODULE_0__[\"TimelineMax\"]();\n  slicktween.fromTo(\"#section-clients\", 0.5, {\n    y: 70,\n    opacity: 0\n  }, {\n    y: 0,\n    opacity: 1\n  });\n  var slickcontroller = new scrollmagic__WEBPACK_IMPORTED_MODULE_1___default.a.Controller();\n  var scene = new scrollmagic__WEBPACK_IMPORTED_MODULE_1___default.a.Scene({\n    triggerElement: \".section-clients\",\n    triggerHook: 0.5\n  }).setTween(slicktween) // .addIndicators()\n  .offset(50).addTo(slickcontroller);\n}; // Our Client\n\n\nvar OurClientAnimation = function OurClientAnimation() {\n  var clienttween = new gsap__WEBPACK_IMPORTED_MODULE_0__[\"TimelineMax\"]();\n  clienttween.fromTo(\"#ourclient\", 0.5, {\n    x: -70,\n    opacity: 0\n  }, {\n    x: 0,\n    opacity: 1\n  });\n  var slickcontroller1 = new scrollmagic__WEBPACK_IMPORTED_MODULE_1___default.a.Controller();\n  var scenee = new scrollmagic__WEBPACK_IMPORTED_MODULE_1___default.a.Scene({\n    triggerElement: \".section-client\",\n    triggerHook: 0.7\n  }).setTween(clienttween) // .addIndicators()\n  .offset(50).addTo(slickcontroller1);\n}; // Business Animation\n\n\nvar BusinessAnimation = function BusinessAnimation() {\n  var tween4 = new gsap__WEBPACK_IMPORTED_MODULE_0__[\"TimelineMax\"]();\n  tween4.fromTo(\"#contact-img\", 0.8, {\n    x: -100,\n    opacity: 0\n  }, {\n    x: 0,\n    opacity: 1\n  }).fromTo(\"#contact-form\", 0.8, {\n    x: 100,\n    opacity: 0\n  }, {\n    x: 0,\n    opacity: 1\n  }, \"-=0.8\");\n  var controller4 = new scrollmagic__WEBPACK_IMPORTED_MODULE_1___default.a.Controller();\n  var scene4 = new scrollmagic__WEBPACK_IMPORTED_MODULE_1___default.a.Scene({\n    triggerElement: \".section-contact \",\n    triggerHook: 0.8\n  }).setTween(tween4).offset(10) // .addIndicators()\n  .addTo(controller4);\n};\n\nvar FooterLogoAnimation = function FooterLogoAnimation() {\n  var tween5 = new gsap__WEBPACK_IMPORTED_MODULE_0__[\"TimelineMax\"]();\n  tween5.fromTo(\"#footer-contact\", 0.8, {\n    y: -40,\n    opacity: 0\n  }, {\n    y: 0,\n    opacity: 1\n  }).fromTo(\"#footer-support\", 1, {\n    y: 40,\n    opacity: 0\n  }, {\n    y: 0,\n    opacity: 1\n  }, \"-=1\").fromTo(\"#footer-logo\", 1, {\n    y: -40,\n    opacity: 0\n  }, {\n    y: 0,\n    opacity: 1,\n    ease: Bounce.easeOut\n  }, \"-=1\");\n  var controller5 = new scrollmagic__WEBPACK_IMPORTED_MODULE_1___default.a.Controller();\n  var scene5 = new scrollmagic__WEBPACK_IMPORTED_MODULE_1___default.a.Scene({\n    triggerElement: \".footer--home\",\n    triggerHook: 0.8\n  }).setTween(tween5).offset(50) // .addIndicators()\n  .addTo(controller5);\n};\n\nvar AboutAnimation = function AboutAnimation() {\n  var t2 = new gsap__WEBPACK_IMPORTED_MODULE_0__[\"TimelineMax\"]();\n  t2.fromTo(\"#missionvision\", 0.5, {\n    x: -30,\n    opacity: 0\n  }, {\n    x: 0,\n    opacity: 1\n  }).fromTo(\"#missionblock\", 0.5, {\n    x: -30,\n    opacity: 0\n  }, {\n    x: 0,\n    opacity: 1\n  }).fromTo(\"#visionblock\", 0.5, {\n    x: 30,\n    opacity: 0\n  }, {\n    x: 0,\n    opacity: 1\n  }, \"-=.5\");\n  var aboutcontroller1 = new scrollmagic__WEBPACK_IMPORTED_MODULE_1___default.a.Controller();\n  var aboutscene1 = new scrollmagic__WEBPACK_IMPORTED_MODULE_1___default.a.Scene({\n    triggerElement: \".section__contents \",\n    triggerHook: 0.6\n  }).setTween(t2).offset(50) // .addIndicators()\n  .addTo(aboutcontroller1);\n};\n\nvar AboutHeaderAnimation = function AboutHeaderAnimation() {\n  var t3 = new gsap__WEBPACK_IMPORTED_MODULE_0__[\"TimelineMax\"]();\n  t3.fromTo(\"#aboutus\", 1, {\n    x: -40,\n    opacity: 0\n  }, {\n    x: 0,\n    opacity: 1\n  }).fromTo(\"#about-head\", 0.5, {\n    x: 40,\n    opacity: 0\n  }, {\n    x: 0,\n    opacity: 1\n  }, \"-=1\").fromTo(\"#about-info\", 0.5, {\n    x: -40,\n    opacity: 0\n  }, {\n    x: 0,\n    opacity: 1\n  }, \"-=1\");\n  var aboutheadercontroller = new scrollmagic__WEBPACK_IMPORTED_MODULE_1___default.a.Controller();\n  var aboutscene2 = new scrollmagic__WEBPACK_IMPORTED_MODULE_1___default.a.Scene({\n    triggerElement: \".header--middle\",\n    triggerHook: 0.5\n  }).setTween(t3).offset(100) // .addIndicators()\n  .addTo(aboutheadercontroller);\n};\n\nvar ValuesAnimation = function ValuesAnimation() {\n  var t4 = new gsap__WEBPACK_IMPORTED_MODULE_0__[\"TimelineMax\"]();\n  t4.fromTo(\"#ourvalues\", 0.5, {\n    y: -50,\n    opacity: 0\n  }, {\n    y: 0,\n    opacity: 1\n  }).fromTo(\".values--heading\", 0.5, {\n    y: -50,\n    opacity: 0\n  }, {\n    y: 0,\n    opacity: 1\n  }).fromTo(\".values--shape__2\", 0.8, {\n    x: -150,\n    opacity: 0\n  }, {\n    x: 0,\n    opacity: 1\n  }, \"-=0.8\").fromTo(\".values--shape__3\", 0.8, {\n    x: -100,\n    opacity: 0\n  }, {\n    x: 0,\n    opacity: 1\n  }, \"-=0.8\").fromTo(\".values--shape__4\", 0.8, {\n    y: -50,\n    opacity: 0\n  }, {\n    y: 0,\n    opacity: 1\n  }, \"-=0.8\").fromTo(\".values--shape__5\", 0.8, {\n    x: 100,\n    opacity: 0\n  }, {\n    x: 0,\n    opacity: 1\n  }, \"-=0.8\").fromTo(\".values--shape__6\", 0.8, {\n    x: 150,\n    opacity: 0\n  }, {\n    x: 0,\n    opacity: 1\n  }, \"-=0.8\");\n  var valuescontroller = new scrollmagic__WEBPACK_IMPORTED_MODULE_1___default.a.Controller();\n  var scene4 = new scrollmagic__WEBPACK_IMPORTED_MODULE_1___default.a.Scene({\n    triggerElement: \"#aboutvalue\",\n    triggerHook: 0.7\n  }).setTween(t4) // .addIndicators()\n  .offset(80).addTo(valuescontroller);\n};\n\n\n\n//# sourceURL=webpack:///./src/js/components/Animation.js?");

/***/ }),

/***/ "./src/js/components/Date.js":
/*!***********************************!*\
  !*** ./src/js/components/Date.js ***!
  \***********************************/
/*! no static exports found */
/***/ (function(module, exports) {

eval("var moonLanding = new Date();\ndate = moonLanding.getFullYear();\ndocument.getElementById(\"date\").innerHTML = date;\n\n//# sourceURL=webpack:///./src/js/components/Date.js?");

/***/ }),

/***/ "./src/js/components/Particle.js":
/*!***************************************!*\
  !*** ./src/js/components/Particle.js ***!
  \***************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

eval("__webpack_require__(/*! particles.js */ \"./node_modules/particles.js/particles.js\");\n\nparticlesJS(\"particles-js\", {\n  \"particles\": {\n    \"number\": {\n      \"value\": 204,\n      \"density\": {\n        \"enable\": true,\n        \"value_area\": 1500\n      }\n    },\n    \"color\": {\n      \"value\": \"#3896fa\"\n    },\n    \"shape\": {\n      \"type\": \"circle\",\n      \"stroke\": {\n        \"width\": 0,\n        \"color\": \"#000000\"\n      },\n      \"polygon\": {\n        \"nb_sides\": 5\n      },\n      \"image\": {\n        \"src\": \"img/github.svg\",\n        \"width\": 1200,\n        \"height\": 1200\n      }\n    },\n    \"opacity\": {\n      \"value\": 0.9390856935424045,\n      \"random\": false,\n      \"anim\": {\n        \"enable\": false,\n        \"speed\": 1,\n        \"opacity_min\": 0.08791208791208792,\n        \"sync\": false\n      }\n    },\n    \"size\": {\n      \"value\": 8,\n      \"random\": true,\n      \"anim\": {\n        \"enable\": false,\n        \"speed\": 40,\n        \"size_min\": 0.1,\n        \"sync\": false\n      }\n    },\n    \"line_linked\": {\n      \"enable\": true,\n      \"distance\": 150,\n      \"color\": \"#4c88f5\",\n      \"opacity\": 0.6313181133058181,\n      \"width\": 2.5252724532232724\n    },\n    \"move\": {\n      \"enable\": true,\n      \"speed\": 3,\n      \"direction\": \"none\",\n      \"random\": true,\n      \"straight\": false,\n      \"out_mode\": \"out\",\n      \"bounce\": false,\n      \"attract\": {\n        \"enable\": false,\n        \"rotateX\": 710.2328774690454,\n        \"rotateY\": 1341.5509907748635\n      }\n    }\n  },\n  \"interactivity\": {\n    \"detect_on\": \"canvas\",\n    \"events\": {\n      \"onhover\": {\n        \"enable\": false,\n        \"mode\": \"repulse\"\n      },\n      \"onclick\": {\n        \"enable\": true,\n        \"mode\": \"push\"\n      },\n      \"resize\": true\n    },\n    \"modes\": {\n      \"grab\": {\n        \"distance\": 400,\n        \"line_linked\": {\n          \"opacity\": 1\n        }\n      },\n      \"bubble\": {\n        \"distance\": 400,\n        \"size\": 40,\n        \"duration\": 2,\n        \"opacity\": 8,\n        \"speed\": 3\n      },\n      \"repulse\": {\n        \"distance\": 200,\n        \"duration\": 0.4\n      },\n      \"push\": {\n        \"particles_nb\": 4\n      },\n      \"remove\": {\n        \"particles_nb\": 2\n      }\n    }\n  },\n  \"retina_detect\": true\n});\n\n//# sourceURL=webpack:///./src/js/components/Particle.js?");

/***/ }),

/***/ "./src/js/components/ScrollToTop.js":
/*!******************************************!*\
  !*** ./src/js/components/ScrollToTop.js ***!
  \******************************************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var jquery__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! jquery */ \"jquery\");\n/* harmony import */ var jquery__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(jquery__WEBPACK_IMPORTED_MODULE_0__);\n\n\nwindow.onscroll = function () {\n  scrollFunction();\n};\n\nfunction scrollFunction() {\n  if (document.body.scrollTop > 20 || document.documentElement.scrollTop > 20) {\n    jquery__WEBPACK_IMPORTED_MODULE_0___default()('#scrollBtn').css('display', 'block');\n  } else {\n    jquery__WEBPACK_IMPORTED_MODULE_0___default()('#scrollBtn').css('display', 'none');\n  }\n}\n\njquery__WEBPACK_IMPORTED_MODULE_0___default()('#scrollBtn').click(function () {\n  jquery__WEBPACK_IMPORTED_MODULE_0___default()('html, body').animate({\n    scrollTop: 0\n  }, 1000);\n});\n\n//# sourceURL=webpack:///./src/js/components/ScrollToTop.js?");

/***/ }),

/***/ "./src/js/components/ShapeOverlay.js":
/*!*******************************************!*\
  !*** ./src/js/components/ShapeOverlay.js ***!
  \*******************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"default\", function() { return ShapeOverlays; });\nfunction _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError(\"Cannot call a class as a function\"); } }\n\nfunction _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if (\"value\" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }\n\nfunction _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }\n\n//\n// these easing functions are based on the code of glsl-easing module.\n// https://github.com/glslify/glsl-easings\n//\nvar ease = {\n  exponentialIn: function exponentialIn(t) {\n    return t == 0.0 ? t : Math.pow(2.0, 10.0 * (t - 1.0));\n  },\n  exponentialOut: function exponentialOut(t) {\n    return t == 1.0 ? t : 1.0 - Math.pow(2.0, -10.0 * t);\n  },\n  exponentialInOut: function exponentialInOut(t) {\n    return t == 0.0 || t == 1.0 ? t : t < 0.5 ? +0.5 * Math.pow(2.0, 20.0 * t - 10.0) : -0.5 * Math.pow(2.0, 10.0 - t * 20.0) + 1.0;\n  },\n  sineOut: function sineOut(t) {\n    var HALF_PI = 1.5707963267948966;\n    return Math.sin(t * HALF_PI);\n  },\n  circularInOut: function circularInOut(t) {\n    return t < 0.5 ? 0.5 * (1.0 - Math.sqrt(1.0 - 4.0 * t * t)) : 0.5 * (Math.sqrt((3.0 - 2.0 * t) * (2.0 * t - 1.0)) + 1.0);\n  },\n  cubicIn: function cubicIn(t) {\n    return t * t * t;\n  },\n  cubicOut: function cubicOut(t) {\n    var f = t - 1.0;\n    return f * f * f + 1.0;\n  },\n  cubicInOut: function cubicInOut(t) {\n    return t < 0.5 ? 4.0 * t * t * t : 0.5 * Math.pow(2.0 * t - 2.0, 3.0) + 1.0;\n  },\n  quadraticOut: function quadraticOut(t) {\n    return -t * (t - 2.0);\n  },\n  quarticOut: function quarticOut(t) {\n    return Math.pow(t - 1.0, 3.0) * (1.0 - t) + 1.0;\n  }\n};\n\nvar ShapeOverlays =\n/*#__PURE__*/\nfunction () {\n  function ShapeOverlays(elm) {\n    _classCallCheck(this, ShapeOverlays);\n\n    this.elm = elm;\n    this.path = elm.querySelectorAll('path');\n    this.numPoints = 2;\n    this.duration = 600;\n    this.delayPointsArray = [];\n    this.delayPointsMax = 0;\n    this.delayPerPath = 200;\n    this.timeStart = Date.now();\n    this.isOpened = false;\n    this.isAnimating = false;\n  }\n\n  _createClass(ShapeOverlays, [{\n    key: \"toggle\",\n    value: function toggle() {\n      this.isAnimating = true;\n\n      for (var i = 0; i < this.numPoints; i++) {\n        this.delayPointsArray[i] = 0;\n      }\n\n      if (this.isOpened === false) {\n        this.open();\n      } else {\n        this.close();\n      }\n    }\n  }, {\n    key: \"open\",\n    value: function open() {\n      this.isOpened = true;\n      this.elm.classList.add('is-opened');\n      this.timeStart = Date.now();\n      this.renderLoop();\n    }\n  }, {\n    key: \"close\",\n    value: function close() {\n      this.isOpened = false;\n      this.elm.classList.remove('is-opened');\n      this.timeStart = Date.now();\n      this.renderLoop();\n    }\n  }, {\n    key: \"updatePath\",\n    value: function updatePath(time) {\n      var points = [];\n\n      for (var i = 0; i < this.numPoints; i++) {\n        var thisEase = this.isOpened ? i == 1 ? ease.cubicOut : ease.cubicInOut : i == 1 ? ease.cubicInOut : ease.cubicOut;\n        points[i] = thisEase(Math.min(Math.max(time - this.delayPointsArray[i], 0) / this.duration, 1)) * 100;\n      }\n\n      var str = '';\n      str += this.isOpened ? \"M 0 0 V \".concat(points[0], \" \") : \"M 0 \".concat(points[0], \" \");\n\n      for (var i = 0; i < this.numPoints - 1; i++) {\n        var p = (i + 1) / (this.numPoints - 1) * 100;\n        var cp = p - 1 / (this.numPoints - 1) * 100 / 2;\n        str += \"C \".concat(cp, \" \").concat(points[i], \" \").concat(cp, \" \").concat(points[i + 1], \" \").concat(p, \" \").concat(points[i + 1], \" \");\n      }\n\n      str += this.isOpened ? \"V 0 H 0\" : \"V 100 H 0\";\n      return str;\n    }\n  }, {\n    key: \"render\",\n    value: function render() {\n      if (this.isOpened) {\n        for (var i = 0; i < this.path.length; i++) {\n          this.path[i].setAttribute('d', this.updatePath(Date.now() - (this.timeStart + this.delayPerPath * i)));\n        }\n      } else {\n        for (var i = 0; i < this.path.length; i++) {\n          this.path[i].setAttribute('d', this.updatePath(Date.now() - (this.timeStart + this.delayPerPath * (this.path.length - i - 1))));\n        }\n      }\n    }\n  }, {\n    key: \"renderLoop\",\n    value: function renderLoop() {\n      var _this = this;\n\n      this.render();\n\n      if (Date.now() - this.timeStart < this.duration + this.delayPerPath * (this.path.length - 1) + this.delayPointsMax) {\n        requestAnimationFrame(function () {\n          _this.renderLoop();\n        });\n      } else {\n        this.isAnimating = false;\n      }\n    }\n  }]);\n\n  return ShapeOverlays;\n}();\n\n\n\n(function () {\n  var elmHamburger = document.querySelector('.hamburger');\n  var gNavItems = document.querySelectorAll('.global-menu__item');\n  var elmOverlay = document.querySelector('.shape-overlays');\n  var overlay = new ShapeOverlays(elmOverlay);\n  elmHamburger.addEventListener('click', function () {\n    if (overlay.isAnimating) {\n      return false;\n    }\n\n    overlay.toggle();\n\n    if (overlay.isOpened === true) {\n      elmHamburger.classList.add('is-opened-navi');\n\n      for (var i = 0; i < gNavItems.length; i++) {\n        gNavItems[i].classList.add('is-opened');\n      }\n    } else {\n      elmHamburger.classList.remove('is-opened-navi');\n\n      for (var i = 0; i < gNavItems.length; i++) {\n        gNavItems[i].classList.remove('is-opened');\n      }\n    }\n  });\n})();\n\n//# sourceURL=webpack:///./src/js/components/ShapeOverlay.js?");

/***/ }),

/***/ "./src/js/contact.js":
/*!***************************!*\
  !*** ./src/js/contact.js ***!
  \***************************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var jquery__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! jquery */ \"jquery\");\n/* harmony import */ var jquery__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(jquery__WEBPACK_IMPORTED_MODULE_0__);\n/* harmony import */ var bootstrap_dist_js_bootstrap__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! bootstrap/dist/js/bootstrap */ \"./node_modules/bootstrap/dist/js/bootstrap.js\");\n/* harmony import */ var bootstrap_dist_js_bootstrap__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(bootstrap_dist_js_bootstrap__WEBPACK_IMPORTED_MODULE_1__);\n/* harmony import */ var _components_Animation__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./components/Animation */ \"./src/js/components/Animation.js\");\n/* harmony import */ var _sass_main_scss__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../sass/main.scss */ \"./src/sass/main.scss\");\n/* harmony import */ var _sass_main_scss__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(_sass_main_scss__WEBPACK_IMPORTED_MODULE_3__);\nvar path = __webpack_require__(/*! path */ \"./node_modules/path-browserify/index.js\");\n\n\n // import 'popper.js';\n// import 'tooltip.js';\n\n__webpack_require__(/*! ./components/ShapeOverlay */ \"./src/js/components/ShapeOverlay.js\");\n\n__webpack_require__(/*! ./components/Particle */ \"./src/js/components/Particle.js\");\n\n__webpack_require__(/*! ./components/ScrollToTop */ \"./src/js/components/ScrollToTop.js\");\n\n__webpack_require__(/*! ./components/Date */ \"./src/js/components/Date.js\");\n\n\nObject(_components_Animation__WEBPACK_IMPORTED_MODULE_2__[\"DialogAnimation\"])();\nObject(_components_Animation__WEBPACK_IMPORTED_MODULE_2__[\"BusinessAnimation\"])();\nObject(_components_Animation__WEBPACK_IMPORTED_MODULE_2__[\"FooterLogoAnimation\"])();\n\njquery__WEBPACK_IMPORTED_MODULE_0___default()(\".hamburger\").click(function () {\n  if (jquery__WEBPACK_IMPORTED_MODULE_0___default()(\".hamburger\").hasClass(\"hamburger-position\")) {\n    jquery__WEBPACK_IMPORTED_MODULE_0___default()(\".hamburger\").removeClass(\"hamburger-position\");\n    jquery__WEBPACK_IMPORTED_MODULE_0___default()(\".navigation__list\").css(\"display\", \"none\");\n  } else {\n    jquery__WEBPACK_IMPORTED_MODULE_0___default()(\".hamburger\").addClass(\"hamburger-position\");\n    jquery__WEBPACK_IMPORTED_MODULE_0___default()(\".navigation__list\").css(\"display\", \"block\");\n  }\n});\n\n//# sourceURL=webpack:///./src/js/contact.js?");

/***/ }),

/***/ "./src/sass/main.scss":
/*!****************************!*\
  !*** ./src/sass/main.scss ***!
  \****************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

eval("// extracted by mini-css-extract-plugin\n\n//# sourceURL=webpack:///./src/sass/main.scss?");

/***/ }),

/***/ "jquery":
/*!*************************!*\
  !*** external "jQuery" ***!
  \*************************/
/*! no static exports found */
/***/ (function(module, exports) {

eval("module.exports = jQuery;\n\n//# sourceURL=webpack:///external_%22jQuery%22?");

/***/ })

/******/ });