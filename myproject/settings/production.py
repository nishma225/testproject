from .base import *

DEBUG = False

ALLOWED_HOSTS = ['*']

# STATIC_ROOT = (os.path.join(BASE_DIR, 'static'))
STATIC_ROOT = os.path.join(os.path.dirname(BASE_DIR), 'static')

# Database
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql',
        'NAME': 'infodev',
        'USER': 'infodev',
        'PASSWORD': 'infodev123',
        'HOST': '127.0.0.1',
        'PORT': '5432',
    }
}

# EMAIL configuration
EMAIL_BACKEND = 'django.core.mail.backends.smtp.EmailBackend'
# EMAIL_BACKEND = 'django.core.mail.backends.console.EmailBackend'
EMAIL_HOST = 'imap.gmail.com'
EMAIL_USE_SSL = True
EMAIL_PORT = 465
EMAIL_HOST_USER = 'noreply@infodev.com.np'
EMAIL_HOST_PASSWORD = 'Nepal999'
